#!/bin/bash

cd docs/ &&\
make clean &&\
cd .. &&\
# Grab version for sphinx conf.py
export SEEQ_API_VERSION=$(./version-01-upgrade.sh print | tail -1) &&\
sphinx-apidoc -M -e -f -o docs/ seeq/ seeq/cli.py &&\
cd docs &&\
make html &&\
cd ..
