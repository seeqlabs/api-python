.. seeq documentation master file, created by
   sphinx-quickstart on Wed Nov 23 16:56:48 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Seeq Python API reference
========================================

``seeq`` is a Python library which enables you to use 
`Seeq <https://seeq.io>`_'s research backend.

Check out the main documentation at `http://docs.seeq.io <http://docs.seeq.io>`_
for:

1. Quickstart
2. Introduction
3. Installation instructions
4. FAQ
5. Support
6. Examples

Api Reference
-------------

.. toctree::
   :maxdepth: 3

   seeq

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

