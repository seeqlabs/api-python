# Standard library imports
import json

# Imports
import pytest
import responses

# Package imports
import seeq

MOCK_HOST = 'https://testing'

def test_url():
    c = seeq.client.Client(MOCK_HOST)
    
    # should be ok
    c.url('register')
    # should die, key does not exist
    with pytest.raises(seeq.errors.URLDataIncompleteError):
        c.url('does_not_exist')
    # should be ok, although unecessary key
    c.url('register', {'test_id': 'test'}) 
    # should die - no key
    with pytest.raises(seeq.errors.URLDataIncompleteError):
        c.url('study') 
    # should die wrong key 
    with pytest.raises(seeq.errors.URLDataIncompleteError):
        c.url('study', {'test_id': 'test'})
    # should be ok
    c.url('study', {'study_id': '123'})

@responses.activate
def test_no_json_error():
    c = seeq.client.Client(MOCK_HOST)

    responses.add(
        responses.POST, 
        c.url('register'),
        body='this is not json', 
        # json={'error':'not_found'}, 
        status=200,
        content_type='application/json'
    )

    with pytest.raises(seeq.errors.NoJSONInResponseError):
        c.register('Name', 'email@email', 'password')

@responses.activate
def test_no_http_error():
    c = seeq.client.Client(MOCK_HOST)

    responses.add(
        responses.POST, 
        c.url('register'),
        body='does not matter what is here', 
        # json={'error':'not_found'}, 
        status=400,
        content_type='application/json'
    )

    with pytest.raises(seeq.errors.HTTPError):
        c.register('Name', 'email@email', 'password')

JWT_OK = 'jwtOK.jwtOK.jwtOK'
JWT_ERROR = 'jwtERR.jwtERR.jwtERR'
JWT_EVIL = 'jwtEVIL.jwtEVIL.jwtEVIL'

REFRESH_TOKEN_OK = 'token_OK'
REFRESH_TOKEN_ERROR = 'token_ERROR'

def frontend_version_min_callback(request):
    if request.headers['Authorization'] == 'JWT {}'.format(JWT_OK):
        return (
            200, 
            {},
            json.dumps({
                'frontent_version_min': '0.1.1.0.0', 
                'message': 'message',
                'message_details': 'message_details'
            })
        )
    else:
        return (401, {}, '{"message":"error"}')

def auth_callback(request):
    '''Gives back a good JWT'''
    payload = json.loads(request.body.decode('utf-8'))
    if payload['refresh_token'] == REFRESH_TOKEN_OK:
        return (
            200, 
            {},
            json.dumps({
                'access_token': JWT_OK
            })
        )
    else:
        return (401, {}, '{"message":"error"}')

def auth_callback_evil(request):
    '''Gives back a BAD JWT'''
    payload = json.loads(request.body.decode('utf-8'))
    if payload['refresh_token'] == REFRESH_TOKEN_OK:
        return (
            200, 
            {},
            json.dumps({
                'access_token': JWT_EVIL
            })
        )
    else:
        return (401, {}, '{"message":"error"}')

@responses.activate
def test_jwt_and_refresh_token_ok():
    c = seeq.client.Client(MOCK_HOST)

    responses.add_callback(
        responses.GET, 
        c.url('frontend_version_min'),
        callback=frontend_version_min_callback,
        content_type='application/json',
    )

    # /auth provides a GOOD auth JWT
    responses.add_callback(
        responses.POST, 
        c.url('auth'),
        callback=auth_callback,
        content_type='application/json',
    )

    c.set_refresh_token(REFRESH_TOKEN_OK)
    # Trigger client to get a new JWT
    c._jwt = JWT_ERROR

    c._frontend_version_min_get()

@responses.activate
def test_jwt_wrong():
    c = seeq.client.Client(MOCK_HOST)

    responses.add_callback(
        responses.GET, 
        c.url('frontend_version_min'),
        callback=frontend_version_min_callback,
        content_type='application/json',
    )

    # /auth provides a BAD auth JWT
    responses.add_callback(
        responses.POST, 
        c.url('auth'),
        callback=auth_callback_evil,
        content_type='application/json',
    )
    
    c.set_refresh_token(REFRESH_TOKEN_OK)
    # Trigger client to get a new JWT
    c._jwt = JWT_ERROR

    with pytest.raises(seeq.errors.HTTPError):
        c._frontend_version_min_get()

@responses.activate
def test_refresh_token_wrong():
    c = seeq.client.Client(MOCK_HOST)

    responses.add_callback(
        responses.GET, 
        c.url('frontend_version_min'),
        callback=frontend_version_min_callback,
        content_type='application/json',
    )

    # /auth provides a GOOD auth JWT
    responses.add_callback(
        responses.POST, 
        c.url('auth'),
        callback=auth_callback,
        content_type='application/json',
    )

    c.set_refresh_token(REFRESH_TOKEN_ERROR)
    # Trigger client to get a new JWT
    c._jwt = JWT_ERROR

    with pytest.raises(seeq.errors.HTTPError):
        c._frontend_version_min_get()